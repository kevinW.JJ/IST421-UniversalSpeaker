package com.universalspeaker.planetexpress;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlanetexpressApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlanetexpressApplication.class, args);
	}
}
