package com.universalspeaker.planetexpress.controller;

import java.io.InputStream;
import java.util.List;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.speech.v1.Speech;
import com.google.api.services.speech.v1.Speech.Builder;
import com.google.api.services.speech.v1.SpeechScopes;
import com.google.api.services.speech.v1.model.RecognitionAudio;
import com.google.api.services.speech.v1.model.RecognitionConfig;
import com.google.api.services.speech.v1.model.RecognizeRequest;
import com.google.api.services.speech.v1.model.SpeechRecognitionAlternative;
import com.google.api.services.speech.v1.model.SpeechRecognitionResult;
import com.google.api.services.translate.TranslateScopes;

@Controller
public class AudioController {
	    @MessageMapping("/audio")
	    @SendTo("/audio/recording")
	    public String recording(String base64Audio) throws Exception {
	        Thread.sleep(1000); // simulated delay
			//Google Credentials
			InputStream credentials = getClass().getClassLoader().getResourceAsStream("auth.json");
			JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
			HttpTransport httpTransport;
			try {
				String[] strippedAudio = base64Audio.split(",");
				httpTransport = GoogleNetHttpTransport.newTrustedTransport();
				GoogleCredential credential = GoogleCredential.fromStream(credentials).createScoped(SpeechScopes.all()).createScoped(TranslateScopes.all());
				credential.refreshToken();
				Builder speech = new Speech.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName("decoder");
				RecognitionConfig recognitionConfig = new RecognitionConfig();
				recognitionConfig.setLanguageCode("en-US");
				RecognitionAudio recognitionAudio = new RecognitionAudio();
				// Using the comma as delimiter to split the blob, element 1 will contain the actual sound data in base64
				recognitionAudio.setContent(strippedAudio[1]);
				// Build the speech with Google credentials (OAuth)
				Speech s = speech.build();
				// Create request
				Speech.SpeechOperations.Recognize recognize = s.speech().recognize(new RecognizeRequest()
						.setConfig(recognitionConfig)
						.setAudio(recognitionAudio));
				List<SpeechRecognitionResult> sList = recognize.execute().getResults();
				for (SpeechRecognitionResult sResult : sList) {
					SpeechRecognitionAlternative alternative = sResult.getAlternatives().get(0);
					return alternative.getTranscript();
				}
				
		    } catch (Exception e) {
//		    	System.out.println(e.getLocalizedMessage());
		    }
	        return "NO RESULT :(";
	        //return recording;
	    }
}
