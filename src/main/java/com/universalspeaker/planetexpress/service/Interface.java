package com.universalspeaker.planetexpress.service;

import com.universalspeaker.planetexpress.model.User;

public class Interface {
	public interface UserService {
		public User findUserByEmail(String email);
		public void saveUser(User user);
	}
}
